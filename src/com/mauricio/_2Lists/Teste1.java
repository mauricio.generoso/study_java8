package com.mauricio._2Lists;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Mauricio
 */
public class Teste1 {

    public static void main(String[] args) {
        
        List<Pessoa> pessoas = new ArrayList<>();
        
        Pessoa p1 = new Pessoa("Maurício", "Generoso", LocalDate.of(1994, 8, 12), "Programador");
        Pessoa p2 = new Pessoa("João", "pé de Feijão", LocalDate.of(2000, 1, 2), "Administrador");
        Pessoa p3 = new Pessoa("Miguel", "pastel", LocalDate.of(1975, 5, 23), "Técnico de Futebol");
        
        pessoas.add(p1);
        pessoas.add(p2);
        pessoas.add(p3);

        System.out.println("Exibe o nome das pessoas: ");
        pessoas.forEach(p -> {System.out.println(p.getNome());});
        System.out.println("");
        
        System.out.println("Exibe o nome das pessoas ordenado: ");
        pessoas.stream().sorted(Comparator.comparing(Pessoa::getNome)).forEach(System.out::println);
        System.out.println("");
        
        System.out.print("A pessoa mais velha é: - FALTA\n");
        System.out.println("");
        
        System.out.println("Ordenado de forma contrária pela profissão: ");
        pessoas.stream().sorted(Comparator.comparing(Pessoa::getProfissao).reversed()).forEach(System.out::println);
        System.out.println("");
        
        
    }
    
}
