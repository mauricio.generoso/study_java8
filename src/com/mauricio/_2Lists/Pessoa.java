package com.mauricio._2Lists;

import java.time.LocalDate;

/**
 *
 * @author Mauricio
 */
public class Pessoa {

    private String nome;
    private String sobrenome;
    private LocalDate nascimento;
    private String profissao;

    public Pessoa(String nome, String sobrenome, LocalDate nascimento, String profissao) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.nascimento = nascimento;
        this.profissao = profissao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public LocalDate getNascimento() {
        return nascimento;
    }

    public void setNascimento(LocalDate nascimento) {
        this.nascimento = nascimento;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    @Override
    public String toString() {
        return "Pessoa{" + "nome=" + nome + ", sobrenome=" + sobrenome + ", nascimento=" + nascimento + ", profissao=" + profissao + '}';
    }

}
