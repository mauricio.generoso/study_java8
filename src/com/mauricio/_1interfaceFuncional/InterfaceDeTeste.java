package com.mauricio._1interfaceFuncional;

/**
 *
 * @author Mauricio Generoso
 */
@FunctionalInterface
public interface InterfaceDeTeste {

    void run();

    default void metodoDefault() {
        System.out.println("Eu sou um método default!!!");
    }

}
