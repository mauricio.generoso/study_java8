package com.mauricio._1interfaceFuncional;

/**
 *
 * @author Mauricio
 */
public class Teste {
    
    public static void main(String[] args) {
        
        SuperClasse interf = new SuperClasse();
        interf.run();
        interf.metodoDefault();
     
        InterfaceDeTeste interfTeste = new SuperClasse();
        interfTeste.run();
        interfTeste.metodoDefault();
        // Aqui o interfTeste não enxerga os métodos do Objeto SuperClasse, pois apesar de ter sido instanciado
        // Como SuperClasse(), ele é do tipo InterfaceDeTeste e não tem os métodos da SuperClasse.
        
        // Já aqui abaixo, é possível acessar.
        SuperClasse subClasse = new SubClasse();
        subClasse.metodoDaSuperClasse();
        // não é possível ter acesso ao método da subClasse aqui.
        
        SubClasse sub = (SubClasse) subClasse; // Se fizer um cast, vai ser possível acessar os métodos.
        sub.metodoDaSubClasse();
    }
    
}
