package com.mauricio._1interfaceFuncional;

/**
 *
 * @author Mauricio
 */
public class SuperClasse implements InterfaceDeTeste {

    @Override
    public void run() {
        System.out.println("Eu sou o método sobrescrito!!!");
    }
    
    public void metodoDaSuperClasse(){
        System.out.println("eu sou o método metodoDaSuperClasse.");
    }

}
