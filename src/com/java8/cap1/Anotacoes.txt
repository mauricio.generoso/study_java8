O java 5 veio em 2004 com mudança bastantes significativas, como generics, enums e anotações.
O java 8 veio em 2014, e trouxe também novas mudanças bastante significativas.

Alterações:
-> Lambda;
-> Methodos references;
-> API de collection que tem as mesmas interfaces desde 1998, recebeu um upgrade com a entrada de Streams e métodos default;
-> Outras pequenas mudanças na linguagem.
-> API java.time