package com.java8.cap10;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

/**
 *
 * @author pigo
 */
public class _5DuracaoEPeriodo {

    public static void main(String[] args) {

        // Antes:
        Calendar agora = Calendar.getInstance();
        Calendar outraData = Calendar.getInstance();
        outraData.set(1988, Calendar.JANUARY, 25);

        long diferenca = agora.getTimeInMillis() - outraData.getTimeInMillis();
        long milisegundosDeUmDia = 100 * 60 * 60 * 4;
        long dias = diferenca / milisegundosDeUmDia;

        // Agora a operação pode ser feita:
        LocalDate agora2 = LocalDate.now();
        LocalDate outraData2 = LocalDate.of(1989, Month.JANUARY, 25);
        long dias2 = ChronoUnit.DAYS.between(outraData2, agora2);
        long meses = ChronoUnit.MONTHS.between(outraData2, agora2);
        long anos = ChronoUnit.YEARS.between(outraData2, agora2);
        System.out.printf("%s dias, %s meses e %s anos\n", dias, meses, anos);

        // OU também, uma outra forma é utilizar a classe Period
        LocalDate agora3 = LocalDate.now();
        LocalDate outraData3 = LocalDate.of(1989, Month.JANUARY, 25);
        Period periodo = Period.between(outraData3, agora3);
        System.out.printf("%s dias, %s meses e %s anos\n",
                periodo.getDays(), periodo.getMonths(), periodo.getYears());

        // É possível ter os valores negativos:
        LocalDate agora4 = LocalDate.now();
        LocalDate outraData4 = LocalDate.of(2015, Month.JANUARY, 25);
        Period periodo2 = Period.between(agora4, outraData4);
        System.out.printf("%s dias, %s meses e %s anos\n",
                periodo2.getDays(), periodo2.getMonths(), periodo2.getYears());

        // Para  ajustar basta fazer isso:
        if (periodo2.isNegative()) {
            periodo2 = periodo2.negated();
        }
        System.out.printf("%s dias, %s meses e %s anos\n",
                periodo2.getDays(), periodo2.getMonths(), periodo2.getYears());

        // A Classe Period considera somente datas, para ser considerado tempos deve-se utiliar Duration
        LocalDateTime agora5 = LocalDateTime.now();
        LocalDateTime daquiAUmaHora = LocalDateTime.now().plusHours(1);
        Duration duration = Duration.between(agora5, daquiAUmaHora);
        if (duration.isNegative()) {
            duration = duration.negated();
        }
        System.out.printf("%s horas, %s minutos e %s segundos",
                duration.toHours(), duration.toMinutes(), duration.getSeconds());

    }
}
