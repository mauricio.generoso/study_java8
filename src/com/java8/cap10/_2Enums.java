package com.java8.cap10;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Locale;

/**
 *
 * @author pigo
 */
public class _2Enums {

    public static void main(String[] args) {

        // Foi trocado as constantes do Calendar por Enums.
        // Os dois exmeplos tem o mesmo resultado:
        System.out.println(LocalDate.of(2014, 12, 25));
        System.out.println(LocalDate.of(2014, Month.DECEMBER, 25));

        // Há também métodos auxiliares nos Enums:
        System.out.println(Month.DECEMBER.firstMonthOfQuarter());
        System.out.println(Month.DECEMBER.plus(2));
        System.out.println(Month.DECEMBER.minus(1));
        
        // Para imprmir o nome formatado há o método getDisplayName e a classe Locale
        
        Locale pt = new Locale("pt");
        
        System.out.println(Month.DECEMBER.getDisplayName(TextStyle.FULL, pt)); // Dezembro
        System.out.println(Month.DECEMBER.getDisplayName(TextStyle.SHORT, pt)); // Dez
        

    }

}
