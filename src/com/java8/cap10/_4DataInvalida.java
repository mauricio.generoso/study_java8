package com.java8.cap10;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author pigo
 */
public class _4DataInvalida {

    public static void main(String[] args) {

        // Com calendar ao criar a data 30/02 não é acusado erro, mas o Calendar
        // ajusta a data
        Calendar instante = Calendar.getInstance();
        instante.set(2014, Calendar.FEBRUARY, 30);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
        System.out.println(dateFormat.format(instante.getTime()));

        // Mas não é isso que esperamos, então a API do java 8 ocorre erro
        // LocalDate.of(2014, Month.FEBRUARY, 30);
        // java.time.DateTimeException: Invalid date'FEBRUARY 30'
        // O mesmo ocorre se tentar criar um horário inválido
        // LocalDateTime horaInvalida = LocalDate.now().atTime(25, 0);
        // java.time.DateTimeException: Invalid value for HourOfDay (valid values 0 - 23): 25 .
        
        
        
    }
}
