package com.java8.cap10;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author pigo
 */
public class _3Formatacao {

    public static void main(String[] args) {

        /**
         * Para formatar basta chamar o método format da classe DateTimeFormatter
         */
        // Exemplo de formação de horas:
        
        LocalDateTime agora = LocalDateTime.now();
        System.out.println("Agora: " + agora.format(DateTimeFormatter.ISO_LOCAL_TIME));
        
        /// Pra usar outras formas além das já enumeradas, basta usar ofPattern
        
        LocalDateTime agora2 = LocalDateTime.now();
        System.out.println("Agora 2: " + agora2.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        
        
        
    }
}
