package com.java8.cap10;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;

/**
 * IMPORTANTE: Todo o modelo do "java.time" é imutável.
 *
 * @author pigo
 */
public class _1CalendarNaoMais {

    public static void main(String[] args) {

        // Criar uma data com adicional de um mês
        //Antes
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);

        // Agora:
        LocalDate mesQueVem = LocalDate.now().plusMonths(1);

        // Outros métodos semelhantes podem ser usados, como plusDay, PlusYear.
        // Além disto, é possívl também diminuir, com minus
        LocalDate anoPassado = LocalDate.now().minusYears(1);

        /**
         * IMPORTANTE: A classe LocalDate representa uma data sem horário e nem
         * timezone
         */
        LocalDate agora = LocalDate.now();
        System.out.println("Data agora: " + agora); // 2017-12-24

        // Há também a representação apenas com a hora:
        LocalTime horaAgora = LocalTime.now();
        System.out.println("Hora agora: " + horaAgora); // 14:53:48.686

        // Para representar data e hora é utilizado uma junção dos dois anteriores: LocalDateTime
        // o método atTime() permite informar a hora desejada
        LocalDateTime hojeMeioDia = LocalDate.now().atTime(12, 0);
        System.out.println("Hoje meio dia: " + hojeMeioDia);

        LocalTime timeAgora = LocalTime.now();
        LocalDate dateAgora = LocalDate.now();
        LocalDateTime dataEHora = dateAgora.atTime(timeAgora);
        System.out.println("Data e hora: " + dataEHora);

        // Também é possivel informar a zona com atZone que recebe um ZonedDateTime.
        ZonedDateTime dataComHoraETimeZone = dataEHora.atZone(ZoneId.of("America/Sao_Paulo"));
        System.out.println("Com Timezone: " + dataComHoraETimeZone);

        // Converter:
        LocalDateTime semTimeZone = dataComHoraETimeZone.toLocalDateTime();
        System.out.println("Sem Timezone(Convertido): " + semTimeZone);

        /**
         * O mesmo pode ser feito com o método toLocalDate da classe
         * LocalDateTime , entre diversos outros métodos para conversão.
         */
        // Esses método da nova API do JAVA 8 possuem um método of() para criação.
        LocalDate date = LocalDate.of(2014, 12, 25);

        // Porém é muito comum necessitar criar uma data a partir de uma string.
        // Para esses casos deve ser utilizado o método parse()
        LocalDate dateString = LocalDate.parse("2017-12-25");
        System.out.println("Parseada: " + dateString);

        // Existe também os métodos "with" que são novos e semelhantes aos setters.
        LocalDate dataDoPassado = LocalDate.now().withYear(1988);
        System.out.println("Data do passado: " + dataDoPassado);

        // Existem também métodos para saber se algo acontece antes, agora ou depois.
        LocalDate hoje = LocalDate.now();
        LocalDate amanha = LocalDate.now().plusDays(1);

        System.out.println("Hoje é antes de amanhã: " + hoje.isBefore(amanha));
        System.out.println("Hoje é depois de amanhã: " + hoje.isAfter(amanha));
        System.out.println("Hoje é igual amanhã: " + hoje.isEqual(amanha));

        /**
         * Para comparar data deve-se estaer atento para o TimeZone. Datas
         * numericamente iguais com timeZone diferente são consideradas como
         * diferentes. Por exemplo:
         */
        
        ZonedDateTime tokyo = ZonedDateTime.of(2012, 5, 2, 10, 30, 0, 0, ZoneId.of("Asia/Tokyo"));
        ZonedDateTime saoPaulo = ZonedDateTime.of(2012, 5, 2, 10, 30, 0, 0, ZoneId.of("America/Sao_Paulo"));
        
        System.out.println("Tokyo-São Paulo 1: " + tokyo.isEqual(saoPaulo)); // false
        
        // No exemplo acima apensar de ambas serem exatamente iguais, os timzone estão diferentes.
        // Agora um exemplo com a diferença exata para retornar true:
        
        tokyo = tokyo.plusHours(12);
        System.out.println("Tokyo-São Paulo 2: " + tokyo.isEqual(saoPaulo));
        
    
    }
    
    

}
