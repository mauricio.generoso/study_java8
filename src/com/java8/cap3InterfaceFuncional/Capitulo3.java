package com.java8.cap3InterfaceFuncional;

import com.java8.cap2Lambda.Usuario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import javax.swing.JButton;

/**
 *
 * @author Mauricio
 */
public class Capitulo3 {

    public static void main(String[] args) {

        Usuario user1 = new Usuario("Paulo Silveira", 150);
        Usuario user2 = new Usuario("Rodrigo Turini", 120);
        Usuario user3 = new Usuario("Guilherme Silveira", 190);

        List<Usuario> usuarios = Arrays.asList(user1, user2, user3);

        /**
         * Seguindo o que foi estudado no capítulo 2, a interface
         * Consumer<Usuario>, por exemplo, tem apenas um único método abstrato,
         * o accept. É por isso que, quando se usa o forEach, o compilador sabe
         * exatamente qual método deverá ser implementado como corpo do lambda:
         */
        usuarios.forEach(u -> System.out.println(u.getNome()));

        /**
         * Mas e se a interface Consumer<Usuario> tivesse dois métodos? O fato
         * de essa interface ter apenas um método não foi uma coincidência, mas
         * sim um requisito para que o compilador consiga traduzi-la para uma
         * expressão lambda. Podemos dizer então que toda interface do Java que
         * possui apenas um método abstrato pode ser instanciada como um código
         * lambda! Isso vale até mesmo para as interfaces antigas, pré-Java 8,
         * como por exemplo o Runnable:
         */
        /**
         * public interface Runnable { public abstract void run(); }
         */
        /**
         * Exemplo de uma Thread escrita para contar de 0 a 1000(modo
         * tradicional):
         */
        System.out.println("***Tradicional: ");
        Runnable r = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    System.out.print(i);
                }
                System.out.println();
            }
        };
        new Thread(r).run();

        /**
         * A INTERFACE RUNNABLE TEM APENAS UM MÉTODO, LOGO É UMA INTERFACE
         * FUNCIONAL E PODE SER UTILIZADA COM O LAMBDA:
         */
        System.out.println("***LAMBDA: ");
        new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.print(i);
            }
            System.out.println();
        }).start();

        /**
         * Um outro exemplo é no Java Swing onde se necessitar implementar um
         * evento em um botão, que tradicionalmente é assim:
         */
        JButton button = new JButton();
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("evento do click acionado");
            }
        });

        /**
         * ActionListener também é uma interface funcional e pode ser utilizado
         * com LAMBDA:
         */
        button.addActionListener((event) -> {
            System.out.println("Evento do botão");
        });

        /**
         * Existem outras interfaces funcionais que podem ser utilizadas também,
         * como por exemplo java.util.Comparator, java.util.concurrent.Callable
         * e java.io.FileFilter.
         */
        /**
         * CRIADO A INTERFACE VALIDADOR. Exemplo utilizando-a:
         */
        Validador<String> validadorCEP = new Validador<String>() {
            @Override
            public boolean valida(String t) {
                return t.matches("[0-9]{5}-[0-9]{3}");
            }
        };

        /**
         * Utilizando LAMBDA:
         */
        Validador<String> validadorCEP2 = valor -> {
            return valor.matches("[0-9]{5}-[0-9]{3}");
        };

        /**
         * Quando possuir somente uma instrução o método pode ser diminuido, e é
         * possível também retirar a palavra reservada return:
         */
        Validador<String> validadorCEP3 = valor -> valor.matches("[0-9]{5}-[0-9]{3}");

        /**
         * O compilador não consegue compilar se a interface funcional não
         * estiver envolvida(É obrigatório possuir uma interface funcional para
         * receber o retorno do Lambda) .Por exemplo o código abaixo não é 
         * compilado pois a classe Object não é uma interface funcional.
         */
        /**
         * Object o = () -> { System.out.println("O que sou eu? Que lambda?");
         * };
         */
        /**
         * Isso retorna um error: incompatible types: Object is not a functional
         * interface. Faz sentido, pois o Object não é funcional. É sempre
         * necessário haver a atribuição (ou alguma forma de inferir) daquele
         * lambda para uma interface funcional. Por exemplo com Runnable que é
         * uma interface funcional:
         */
        Runnable o = () -> {
            System.out.println("O que sou eu? Que lambda?");
        };

        /**
         * Mas o que é exatamente retornado para o "Runnable o"?
         */
        System.out.println(o);
        System.out.println(o.getClass());

        /**
         * Nas saídas acima é possível ver que retorna o nome da classe, mas não
         * é criado .class ao compilar pois é feito de forma dinâmica, e. não
         * da mesma forma como é criado o .class nas classes anônimas. Foi
         * optado por utilizar MethodHandles e invokedynamic para isso. Pode ser
         * visto mais detalhes avançados sobre a implementação aqui:
         * http://cr.openjdk.java.net/\char126briangoetz/lambda/lambda-translation.html
         */
        /**
         * No capítulo 12 haverá mais detalhes.
         */
        /**
         * CAPTURA DE VARIÁVEIS LOCAIS.
         */
        final int numero = 5;
        new Thread(() -> System.out.println("Número: " + numero)).start();

        /**
         * É possível também acessar variáveis que não sejam final, porém, a
         * variável não pode ser alterada em nenhum outro lugar do código, como
         * por exemplo abaixo:
         */
        /*
            int numero = 5;
            new Thread(() ->{
                System.out.println(numero);    // não compila
            }).start();
            numero = 10;                       // por causa dessa linha!
         */
        /**
         * Obs: O mesmo conceito acima vale para classes anônimas a partir do
         * Java 8, referente a acessar variáveis fora do escopo.
         */
        
        
    }
}
 