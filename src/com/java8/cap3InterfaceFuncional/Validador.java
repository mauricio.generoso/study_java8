package com.java8.cap3InterfaceFuncional;

/**
 * Criando uma interface funcional
 *
 * @author Mauricio
 * @param <T>
 */
@FunctionalInterface
public interface Validador<T> {

    boolean valida(T t);

    default void imprime() {
        System.out.println("Hello Word");
    }
}

/**
 * Não é necessário fazer nada de diferente, o compilador já entede que trata-se
 * de uma interface funcional.
 */
/**
 * É possível incluir a anotação @FunctionInterface para garantir que a
 * interface seja funcional, e impedir que alguém inclua um novo método fazendo
 * com que a classe deixe de ser funcional. Mas não é preciso a anotação, é
 * apenas para segurança de alterações futuras.
 */
/**
 * Ao compilar uma interface com esta anotação, e possuir mais de um método
 * ABSTRATO, ocorrerá erro de compilação.
 */
/**
 * IMPORTATE: As interfaces podem implementar métodos desde que os mesmo sejam
 * default, conforme no exemplo acima. A interface funcional ocorre quando
 * possui somente um método ABSTRATO, logo pode ter métodos default, trata-se de
 * uma implementação do JAVA 8 para não "quebrar" código já existente.
 */
