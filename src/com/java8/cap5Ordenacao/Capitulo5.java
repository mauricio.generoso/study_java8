package com.java8.cap5Ordenacao;

import com.java8.cap2Lambda.Usuario;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Mauricio
 */
public class Capitulo5 {

    public static void main(String[] args) {

        // COMPARATORS COM LAMBDA
        /**
         * Uma classe que implementa "Comparator" pode ser passada para
         * Collections.sort() para ser ordenado.
         */
        Usuario user1 = new Usuario("Paulo Silveira", 150);
        Usuario user2 = new Usuario("Rodrigo Turini", 120);
        Usuario user3 = new Usuario("Guilherme Silveira", 190);

        List<Usuario> usuarios = Arrays.asList(user1, user2, user3);

        Comparator<Usuario> comparator = new Comparator<Usuario>() {
            @Override
            public int compare(Usuario u1, Usuario u2) {
                return u1.getNome().compareTo(u2.getNome());
            }
        };

        Collections.sort(usuarios, comparator);

        /**
         * Comparator é uma interface funcional, logo poderia ser escrita assim:
         */
        Collections.sort(usuarios, (u1, u2) -> u1.getNome().compareTo(u2.getNome()));

        // MÉTODO List.sort()
        /**
         * Existe um método default em "List" para ordenação e basta passar o
         * Comparator como parâmetro, assim a ordenação fica ainda menor de ser
         * escrita:
         */
        usuarios.sort((u1, u2) -> u1.getNome().compareTo(u2.getNome()));

        // MÉTODOS ESTÁTICOS NAS INTERFACES
        /**
         * OUTRA NOVIDADE DO JAVA 8 é poder ter métodos estáticos dentro das
         * interfaces
         */
        // método estático na interface Comparator, como comparing(...)
        /*
            Comparator.comparing(...), é uma fábrica (factory) de Comparators.
            Comparator<Usuario> comparator = Comparator.comparing(u -> u.getNome());
         */
        usuarios.sort(Comparator.comparing(u -> u.getNome()));
        // Isso cria um comparator que irá utilizar como critério o nome de usuário

        /**
         * E se tivermos uma lista de objetos que implementam Comparable, como
         * por exemplo String? Antes faríamos:
         */
        /*
            List<String> palavras = Arrays.asList("Casa do Código", "Alura", "Caelum");
            Collections.sort(palavras); 
         */
        /**
         * Tentar fazer, no Java 8, palavras.sort() não compila. Não há esse
         * método sort em List que não recebe parâmetro. Nem teria como haver,
         * pois o compilador não teria como saber se o objeto invocado é uma
         * List de Comparable ou de suas filhas. Como resolver? Criar um lambda
         * de Comparator que simplesmente delega o trabalho para o compareTo?
         * Não é necessário, pois isso já existe:
         */
        /*
            List<String> palavras = Arrays.asList("Casa do Código", "Alura", "Caelum");
            palavras.sort(Comparator.naturalOrder()); 
         */
        /**
         * Comparator.naturalOrder() retorna um Comparator que delega para o
         * próprio objeto. Há também o Comparator.reverseOrder().
         */
    }
}
