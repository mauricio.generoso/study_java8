package com.java8.cap6methodReferences;

import com.java8.cap2Lambda.Usuario;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 *  
 * 
 * 
 * @author Mauricio
 */
public class Capitulo6 {

    public static void main(String[] args) {

        Usuario user1 = new Usuario("Paulo Silveira", 150);
        Usuario user2 = new Usuario("Rodrigo Turini", 120);
        Usuario user3 = new Usuario("Guilherme Silveira", 190);

        List<Usuario> usuarios = Arrays.asList(user1, user2, user3);

        // MÉTODOS REFERENCES:
        /**
         * Muitas vezes vamos escrever uma expressão lambda que apenas delega a
         * invocação para um método existente. Por exemplo:
         */
        usuarios.forEach(u -> u.tornarModerador());

        /**
         * Com methods references é possível informar ao forEach qual método
         * deve ser chamado com a sintaxe abaixo:
         */
        usuarios.forEach(Usuario::tornarModerador);

        /**
         * IMPORTANTE: SEMPRE QUE O LAMBDA FOR CHAMAR UM ÚNICO MÉTODO, PODE SER USADO METHOD REFERENCE(****POR BOAS PRÁTICAS, DEVE****)
         */
        
        /**
         * Isso é possível(acima) pois o method reference é traduzido para uma
         * interface funcional. Variação:
         */
        Consumer<Usuario> tornaModerador = Usuario::tornarModerador;
        usuarios.forEach(tornaModerador);

        // METHOD REFERENCE NAS COMPARAÇÕES:
        // Isso:
        usuarios.sort(Comparator.comparing(u -> u.getNome()));
        //Pode ser escrito assim:
        usuarios.sort(Comparator.comparing(Usuario::getNome));

        // Comparação por pontos e na ordem reversa:
        usuarios.sort(Comparator.comparing(Usuario::getPontos).reversed());

        //REFERENCIAS COM MÉTODOS DE INSTANCIA
        Usuario mauricio = new Usuario("Maurício Generoso", 150);
        Runnable bloco = mauricio::tornarModerador;
        bloco.run();

        // Os dois códigos abaixo são equivalentes:
        Runnable bloco1 = mauricio::tornarModerador;
        Runnable bloco2 = () -> mauricio.tornarModerador();

        /**
         * O que foi feito acima é bastante diferente de
         * "Usuario::tornaModerador", pois um referencia qualquer objeto do tipo
         * usuário e outro somente um objeto em específico.
         */
        // REFERENCIANDO MÉTODOS QUE RECEBEM ARGUMENTOS
        // Exemplo:
        System.out.println("***IMPRIME COM METHOD REFERENCE:");
        usuarios.forEach(System.out::println);
        // mas o que irá imprimir?
        /*
            Quando se escreve System.out::println é equivalente a estar 
            escrevendo com LAMBDA: u -> System.out.println(u).
         */

        // O código traduzido para o Java 7, seria:
        
        for (Usuario u : usuarios) {
            System.out.println(u);
        }
        // Então, sempre que tiver somente um método, buscar usar METHOD REFERENCE
        usuarios.forEach(System.out::println);

        
        // REFERENCIANDO CONSTRUTORES:
        // O código abaixo não compila pois é necessário armazenar o retorno em uma interface funcional:
        //Usuario rodrigo = Usuario::new;
        //Será utilizado a interface abaixo para armazenar o retorno:
        /*
            @FunctionalInterface
            public interface Supplier<T>{
                T get();
            }
         */
        Supplier<Usuario> criadorDeUsuarios1 = Usuario::new;
        Usuario novo1 = criadorDeUsuarios1.get();
        System.out.println("Novo: " + novo1);

        // Para inicializar com um construtor padrão que contenha um argumento.
        /**
         * A String em <String, Usuario> é o objeto que será o parâmetro do
         * método aplly, se acessar o código da função Function<R, T>, será
         * possivel visualizar isso. O Usuário é o tipo de retorno da função.
         * Quando se utiliza Usuario::new, é incluido o construtor de um usuário
         * no método aplly() que tem como parâmertro uma String e retorno um
         * usuário, logo, sempre será retornado a instância de um novo usuário.
         */
        Function<String, Usuario> criadorDeUsuarios2 = Usuario::new;
        Usuario rodrigo2 = criadorDeUsuarios2.apply("Rodrigo2");
        System.out.println("Novo2: " + rodrigo2);

        /**
         * Para criar uma fábrica de usuário com dois argumentos, poderia ser
         * utilizado BiFunction que tem o mesmo propósito acima.
         */
        BiFunction<String, Integer, Usuario> criadorDeUsuarios = Usuario::new;

        // É possível inicializar um construtor de array com a sintaxe:
        // int[]::new
        //IMPORTANTE:
        // DEVE-SE TOMAR CUIDADE COM AUTOBOXING E UNBOXING
        
        /**
         * A atenção ao auto boxing desnecessário é constante. Assim como vimos
         * no caso da Function, a BiFunction possui suas interfaces análogas
         * para tipos primitivos. Em vez de usar a BiFunction, poderíamos usar
         * aqui uma ToIntBiFunction<Integer, Integer>, evitando o unboxing do
         * retorno. Sim, é possível evitar todo boxing, usando uma
         * IntBinaryOperator, que recebe dois ints e devolve um int.
         */
    }
}
