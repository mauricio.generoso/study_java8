package com.java8.cap4MetodosDefault;

import com.java8.cap2Lambda.Usuario;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * É possível declarar métodos dentro de interfaces com a palavra reservada
 * "default". Trata-se de um novo recurso do Java 8.
 *
 * @author Mauricio
 */
public class Capitulo4 {

    public static void main(String[] args) {

        /**
         * Continuando o estudo do capítulo 3, até agora engolimos o método
         * forEach invocado em nossa List. De onde ele vem, se até o Java 7 isso
         * não existia? Uma possibilidade seria a equipe do Java tê-lo declarado
         * em uma interface, como na própria List. Qual seria o problema? Todo
         * mundo que criou uma classe que implementa List precisaria
         * implementá-lo. O trabalho seria enorme, mas esse não é o principal
         * ponto. Ao atualizar o seu Java, bibliotecas que têm sua própria List,
         * como o Hibernate, teriam suas implementações quebradas, faltando
         * métodos, podendo gerar os assustadores NoSuchMethodErrors.
         */
        /**
         * Como adicionar um método em uma interface e garantir que todas as
         * implementações o possuam implementado? Com um novo recurso,
         * declarando código dentro de um método de uma interface!
         */
        /**
         * Por exemplo, o método forEach que utilizamos está declarado dentro de
         * java.lang.Iterable, que é mãe de Collection, por sua vez mãe de List.
         * Abrindo seu código-fonte, podemos ver:
         *
         */
        /*
            default void forEach(Consumer<? super T> action) { 
                    Objects.requireNonNull(action);
                    for (T t : this) { 
                        action.accept(t); 
                    } 
            }
         */
        /**
         * Pois é. Método com código dentro de interfaces! Algo que nunca
         * esperaríamos ser possível no Java. Esses métodos são chamados de
         * default methods (na documentação do java pode ser também encontrado
         * defender methods ou ainda extension methods). Como ArrayList
         * implementa List, que é filha (indireta) de Iterable, a ArrayList
         * possui esse método, quer ela queira ou não. É por esse motivo que
         * podemos fazer: usuarios.forEach(u ->
         * System.out.println(u.getNome()));
         */
        /**
         * Reforçando o que foi visto nos capítulos anteriores, uma interface
         * funcional possui somente um método, só que, possui somente um método
         * ABSTRADO, é possível que a interface possua métodos default, sendo
         * assim a mesma ainda poderá continuar a ser compilada sem erros e ser
         * utilzada no LAMBDA como uma interface funcional.
         */
        /**
         * IMPORTANTE: A própria interface CONSUMER<T> possui um método default
         */
        /**
         * O código abaixo fará com que uma implementação de consumer seja
         * chamada e depois a próxima.
         */
        Usuario user1 = new Usuario("Paulo Silveira", 150);
        Usuario user2 = new Usuario("Rodrigo Turini", 120);
        Usuario user3 = new Usuario("Guilherme Silveira", 190);

        List<Usuario> usuarios = Arrays.asList(user1, user2, user3);

        Consumer<Usuario> mostraMensagem = u
                -> System.out.println("antes de imprimir os nomes");

        Consumer<Usuario> imprimeNome = u
                -> System.out.println(u.getNome());

        usuarios.forEach(mostraMensagem.andThen(imprimeNome));

        /**
         * Com a implementação acima é possível por exemplo, gerar um log de um
         * usuário que fez alguma coisa.
         */
        //===========================================
        /**
         * outro método: removeIf
         */
        /**
         * O método removeIf recebe um Predicate que é uma interface funcional,
         * que permite testar objetos, e o removeIf irá remover todos os Objetos
         * que retornarem true no Predicate.
         */
        /* Por exemplo, para remover os usuários com mais de 100 pontos:*/
        System.out.println("***PREDICATE: ");
        List<Usuario> usuarios2 = new ArrayList<>();
        usuarios2.add(user1);
        usuarios2.add(user2);
        usuarios2.add(user3);

        Predicate<Usuario> predicado = new Predicate<Usuario>() {
            @Override
            public boolean test(Usuario u) {
                return u.getPontos() > 160;
            }
        };

        usuarios2.removeIf(predicado);
        usuarios2.forEach(u -> System.out.println(u.getNome()));

        /**
         * Importante: Há um detalhe aqui: o removeIf invoca o remove de uma
         * coleção, então a lista não pode ser imutável(inalterada), ou um
         * UnsupportedOperationException será lançado indicando operação não
         * suportada. Esse é o caso da lista devolvida por Arrays.asList. Por
         * isso, foi necessário utilizar uma coleção mutável como ArrayList.
         */
        /**
         * Pode-se também eliminar a necessidade de criar uma variável predicado
         * e fazer direto com LAMBDA:
         */
        usuarios2.removeIf(u -> u.getPontos() > 160);

    }
}
