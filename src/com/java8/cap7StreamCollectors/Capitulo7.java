package com.java8.cap7StreamCollectors;

import com.java8.cap2Lambda.Usuario;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;

/**
 *
 * @author Mauricio
 */
public class Capitulo7 {

    public static void main(String[] args) {

        // Será visto o avanço na API Collections
        Usuario user1 = new Usuario("Paulo Silveira", 150);
        Usuario user2 = new Usuario("Rodrigo Turini", 120);
        Usuario user3 = new Usuario("Guilherme Silveira", 190);

        List<Usuario> usuarios = Arrays.asList(user1, user2, user3);

        // Filtrar os 10 usuários com mais pontos e torná-los moderador.
        usuarios.sort(Comparator.comparing(Usuario::getPontos).reversed());
        usuarios.subList(0, 3).forEach(Usuario::tornarModerador);

        // Antes do Java 8 seria necessário fazer:
        Collections.sort(usuarios, new Comparator<Usuario>() {
            @Override
            public int compare(Usuario u1, Usuario u2) {
                return u1.getPontos() - u2.getPontos();
            }
        });

        Collections.reverse(usuarios);
        List<Usuario> top10 = usuarios.subList(0, 3);
        for (Usuario usuario : top10) {
            usuario.tornarModerador();
        }

        // Agora para tornar moderadores todos os usuários com mais de 100 
        // pontos, como é feito? Com Stream.
        // De forma tradicional pode ser feito:
        for (Usuario usuario : usuarios) {
            if (usuario.getPontos() > 200) {
                usuario.tornarModerador();
            }
        }

        /**
         * A API de Collections é muito grande e fica difícil de evoluí=la,
         * então foi criado Stream para poder filtrar de uma melhor forma com um
         * novo recurso do java 8. *
         */
        // Como criar uma Stream que represente os elementos da Collections? Foi criado um método default stream();
        // A partir do Stream é possível utilizar o método filter();
        Stream<Usuario> stream = usuarios.stream();
        stream.filter(u -> u.getPontos() > 100);

        // Além disto é possível eliminar a variável temporária.
        usuarios.stream().filter(u -> u.getPontos() > 100);

        /**
         * IMPORTANTE: O Stream não altera a coleção original, se filtrar a
         * lista e depois imprimí-la, todos os elementos serão impressos.
         */
        // Sendo assim, voltando ao problema anterior, filtrar os usuários 
        // com mais de 100 pontos e torná-los moderador:
        usuarios.stream().filter(u -> u.getPontos() > 100)
                .forEach(Usuario::tornarModerador);

        usuarios.forEach(System.out::println); // Chama o toString do usuário

        // Como armazenar a lista filtrada? É necessário uma nova lista.
        List<Usuario> maisQue100 = new ArrayList<>();
        usuarios.stream().filter(u -> u.getPontos() > 100)
                .forEach(u -> maisQue100.add(u));

        // ainda pode ser simplificado com method reference:
        usuarios.stream().filter(u -> u.getPontos() > 100)
                .forEach(maisQue100::add);

        // Como isso é algo que ocorre com frequencia, há uma forma mais simples:
        // COLLECTORS
        /**
         * Pode-se utilizar o método collect para resgatar os elementos de
         * Stream<Usuario>, porém a assinatura do método recebe três argumentos,
         * três interfaces funcionais, Supplier<R>, que é uma factory que vai
         * criar o objeto que será devolvido no final da coleta.
         * BiConsumer<R, ? super T>, que é o método que será invocado para
         * inserir cada elemento, e BiConsumer<R, R>, que pode ser invocado se
         * precisar inserir mais de um elemento ao mesmo tempo. Seria necessário
         * um código como este:
         */
        Supplier<ArrayList<Usuario>> supplier = ArrayList::new;
        BiConsumer<ArrayList<Usuario>, Usuario> accumulator
                = ArrayList::add;
        BiConsumer<ArrayList<Usuario>, ArrayList<Usuario>> combiner
                = ArrayList::addAll;

        List<Usuario> maisQue100DeNovo = usuarios.stream()
                .filter(u -> u.getPontos() > 100)
                .collect(supplier, accumulator, combiner);

        // Poderia ser simplificado conforme abaixo:
        usuarios.stream()
                .filter(u -> u.getPontos() > 100)
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);

        /**
         * Há uma outra opção de se realizar esta rotina com o método collect,
         * recebendo um Collector como parâmetro:
         */
        // Sintaxe: <R, A> R collect(Collector<? super T, A, R> collector);
        //Ficaria assim:
        usuarios.stream()
                .filter(u -> u.getPontos() > 100)
                .collect(Collectors.toList());

        // Agora é possível obter as informações da lista:
        List<Usuario> maisQue100DeNovo2 = usuarios.stream()
                .filter(u -> u.getPontos() > 100)
                .collect(toList());

        // Existe o método toCollection que permite escolher a implementação que será devolvida.
        Set<Usuario> set = usuarios.stream().collect(Collectors.toCollection(HashSet::new));

        // ou:
        Usuario[] array = usuarios.stream().toArray(Usuario[]::new);

        // No Java 8 foi introduzido também a classe Optional
        OptionalDouble media = usuarios.stream().mapToInt(Usuario::getPontos).average();

        // Para evitar erros:
        double media2 = usuarios.stream().mapToInt(Usuario::getPontos).average().orElse(0.0);

        // Ou também é possível gerar Exception
        double media3 = usuarios.stream().mapToInt(Usuario::getPontos).average().orElseThrow(IllegalStateException::new);
        
        // Outros:
        Optional<Usuario> max = usuarios.stream().max(Comparator.comparingInt(Usuario::getPontos));
        
        

    }

}
