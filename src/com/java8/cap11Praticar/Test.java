package com.java8.cap11Praticar;

import java.math.BigDecimal;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import static java.util.Arrays.asList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Mauricio Generoso
 */
public class Test {

    public static void main(String[] args) {

        Customer paulo = new Customer("Paulo Silveira");
        Customer rodrigo = new Customer("Rodrigo Turini");
        Customer guilherme = new Customer("Guilherme Silveira");
        Customer adriano = new Customer("Adriano Almeida");

        Product bach = new Product("Bach Completo",
                Paths.get("/music/bach.mp3"), new BigDecimal(100));
        Product poderosas = new Product("Poderosas Anita",
                Paths.get("/music/poderosas.mp3"), new BigDecimal(90));
        Product bandeira = new Product("Bandeira Brasil",
                Paths.get("/images/brasil.jpg"), new BigDecimal(50));
        Product beauty = new Product("Beleza Americana",
                Paths.get("beauty.mov"), new BigDecimal(150));
        Product vingadores = new Product("Os Vingadores",
                Paths.get("/movies/vingadores.mov"), new BigDecimal(200));
        Product amelie = new Product("Amelie Poulain",
                Paths.get("/movies/amelie.mov"), new BigDecimal(100));

        LocalDateTime today = LocalDateTime.now();
        LocalDateTime yesterday = today.minusDays(1);
        LocalDateTime lastMonth = today.minusMonths(1);

        Payment payment1 = new Payment(asList(bach, poderosas), today, paulo);
        Payment payment2 = new Payment(asList(bach, bandeira, amelie), yesterday, rodrigo);
        Payment payment3 = new Payment(asList(beauty, vingadores, bach), today, adriano);
        Payment payment4 = new Payment(asList(bach, poderosas, amelie), lastMonth, guilherme);
        Payment payment5 = new Payment(asList(beauty, amelie), yesterday, paulo);

        List<Payment> payments = asList(payment1, payment2,
                payment3, payment4, payment5);

        // 11.2 Ordenando os pagamentos
        System.out.println("Pagamentos ordenados por data: ");
        payments.stream()
                .sorted(Comparator.comparing(Payment::getDate))
                .forEach(System.out::println);

        // 11.3 Reduzindo BigDecimal em somas
        // calcular o valor de payment
        System.out.print("Soma dos pagamentos 1: ");
        payment1.getProducts().stream()
                .map(Product::getPrice)
                .reduce(BigDecimal::add)
                .ifPresent(System.out::println);

        // Se usar o Reduce que recebe o valor inicial fica:
        System.out.print("Soma dos pagamentos 1 de novo: ");
        BigDecimal total = payment1.getProducts().stream()
                .map(Product::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(total);

        // Somar todos os pagamentos
        Stream<BigDecimal> pricesStream
                = payments.stream()
                        .map(p -> p.getProducts().stream()
                        .map(Product::getPrice)
                        .reduce(BigDecimal.ZERO, BigDecimal::add));

        BigDecimal totalPayments = payments.stream()
                .map(p -> p.getProducts().stream()
                .map(Product::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add))
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("Total de pagamentos: " + totalPayments);

        // O código acima está bastante repetitivo, pode ser usado um flatMap:
        Stream<BigDecimal> priceOfEachProduct
                = payments.stream()
                        .flatMap(p -> p.getProducts().stream().map(Product::getPrice));

        // para somar bastaria adicionar o reduce, então o código acima ficaria:
        BigDecimal totalFlat = payments.stream()
                .flatMap(p -> p.getProducts().stream().map(Product::getPrice))
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        // 14.4 - Produtos mais vendidos
        Stream<Product> products = payments.stream()
                .map(Payment::getProducts)
                .flatMap(List::stream);

        Map<Product, Long> topProducts = payments.stream()
                .flatMap(p -> p.getProducts().stream())
                .collect(Collectors.groupingBy(Function.identity(),
                        Collectors.counting()));

        System.out.println(topProducts);

    }
}
