package com.java8.cap2Lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 *
 * @author Mauricio
 */
public class Capitulo2 {

    public static void main(String[] args) {

        Usuario user1 = new Usuario("Paulo Silveira", 150);
        Usuario user2 = new Usuario("Rodrigo Turini", 120);
        Usuario user3 = new Usuario("Guilherme Silveira", 190);

        List<Usuario> usuarios = Arrays.asList(user1, user2, user3);

        /**
         * Imprimindo os usuários da forma tradicional
         */
        System.out.println("***Modo tradicional");
        for (Usuario u : usuarios) {
            System.out.println(u.getNome());
        }
        /**
         * O forEach é utilizado desde o java 5, pode ser usado com qualquer
         * Objeto que implemente a interface Iterable.
         * 
         *  O forEach é implementado na interface Iterable como um método 
         * default e internamente faz o mesmo que o forEach tradicional.
         */

        //====================================================
        /**
         * A partir do java 8, se tem o método forEach de uma outra forma:
         * usuarios.forEach(...) O parâmratro que esse método recebe é um objeto
         * do tipo java.util.function.Consumer, que um único método, o accept.
         *
         * Trata de uma nova interface do java 8 assim como qualquer interface
         * no pacote java.util.function.
         *
         * A classe Mostrador é um exemplo dessa interface sendo utilizada. Ela
         * implementa o método accept e consome o usuário recebido por
         * parâmetro(consome = fazz alguma coisa)
         *
         * Com isso, é possível instanciar esta classe e passar o argumento para
         * o novo forEach(), como no exemplo:
         */
        System.out.println("***Novo modo: ");
        Mostrador mostrador = new Mostrador();
        usuarios.forEach(mostrador);
        
        // Ao criar um interface que implementa o forEach, qualquer coisa pode
        // ser feita no método accept(), logo.

        /**
         * Seria possível também criar a classe de forma anônima:
         */
        System.out.println("***Novo modo de outra forma: ");
        Consumer<Usuario> mostrador2 = new Consumer<Usuario>() {
            @Override
            public void accept(Usuario t) {
                System.out.println(t.getNome());
            }
        };
        usuarios.forEach(mostrador2);

        /**
         * Porém o código ainda está grande, é possível eliminar a necessidade
         * de criar uma variável conforme segue:
         */
        System.out.println("***Sem criar variável");
        usuarios.forEach(new Consumer<Usuario>() {
            @Override
            public void accept(Usuario t) {
                System.out.println(t.getNome());
            }
        });

        //===========LAMBDA==============
        /**
         * No java 8, o LAMBDA é uma forma simples de implementar uma interface
         * que possui somente um método. Essas interfaces são ditas interfaces
         * funcionais, logo, o LAMBDA somente funciona com interfaces
         * funcionais (com um único método). Ficará assim:
         */
        Consumer<Usuario> mostrador3
                = (Usuario u) -> {
                    System.out.println(u.getNome());
                };

        /**
         * O compilador também entende se retirar o tipo de parâmetro e o
         * parenteses, pois o tipo já está definido em "Customer<Usuario>":
         */
        Consumer<Usuario> mostrador4 = u -> {
            System.out.println(u.getNome());
        };

        /**
         * Caso o bloco dentro de { } contenha apenas uma instrução, podemos
         * omiti-lo e remover também o ponto e vírgula:
         */
        Consumer<Usuario> mostrador5 = u -> System.out.println(u.getNome());

        /**
         * Por fim, é possível declarar este trecho de código dentro do método
         * forEach:
         */
        System.out.println("***LAMBDA: ");
        usuarios.forEach(u -> System.out.println(u.getNome()));
        
        /**
         * É possível também executar outras rotinas com o Lambda, como por
         * exemplo tornar todos os usuários moderadores:
         */
        usuarios.forEach(u -> u.setModerador(true));

        /**
         * IMPORTANTE: Vale lembrar que essa variável "u" não pode ter sido
         * declarada no mesmo escopo da invocação do forEach, pois o lambda pode
         * capturar as variáveis de fora.
         * Isso pode ter suas vantagens mas também há desvantagens. 
         * Será visto mais a frente.
         */
    }

}
