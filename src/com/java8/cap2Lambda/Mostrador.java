package com.java8.cap2Lambda;

import java.util.function.Consumer;

/**
 *
 * @author Mauricio
 */
public class Mostrador implements Consumer<Usuario>{

    @Override
    public void accept(Usuario t) {
        System.out.println(t.getNome());
    }
    
}
