package com.java8.cap8MaisStream;

import com.java8.cap2Lambda.Usuario;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.IntBinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Mauricio
 */
public class Capitulo8 {

    public static void main(String[] args) {

        Usuario user1 = new Usuario("Paulo Silveira", 150);
        Usuario user2 = new Usuario("Rodrigo Turini", 120);
        Usuario user3 = new Usuario("Guilherme Silveira", 190);

        List<Usuario> usuarios = Arrays.asList(user1, user2, user3);

        // Ordernar:
        usuarios.sort(Comparator.comparing(Usuario::getNome));

        // Para ordenar com stream
        // Exemplo filtrar com mais de 100 pontos e ordenar:
        usuarios.stream().filter(u -> u.getPontos() > 100).sorted(Comparator.comparing(Usuario::getNome));

        // IMPORTANTE: A diferença de ordenar com List ou Stream é que Stream não altera a lista original.
        // 8.4
        /* É possível fazer com que o Stream realize alguma tarega sempre que processar algum elemento
         * através do método peek.
          * No exemplo a seguir irá imprimir os elementos processados.
         **/
        usuarios.stream().filter(u -> u.getPontos() > 100)
                .peek(System.out::println)
                .findAny();

        // Outro exemplo:
        usuarios.stream()
                .sorted(Comparator.comparing(Usuario::getNome))
                .peek(System.out::println)
                .collect(Collectors.toList());

        // 8.5 Operações que utilizam valores da Stream para retornar um valor final são chamados de Redução.
        double pontuacaoMedia = usuarios.stream().mapToInt(Usuario::getPontos).average().getAsDouble();

        // Há outros métodos também, como min, max, count e sum. Todos trabalham com Optional
        // O min e o max recebem um Comparator para poder fazer a comparação, por exemplo para saber o que deve buscar com máximo.
        Optional<Usuario> max = usuarios.stream().max(Comparator.comparing(Usuario::getPontos));

        // Exemplo de soma:
        int soma = usuarios.stream().mapToInt(Usuario::getPontos).sum();

        // É possível ainda colocar um valor inicial e explanar mais como funciona a soma:
        int valorInicial = 0;
        IntBinaryOperator operacao = (a, b) -> a + b;

        int total = usuarios.stream()
                .mapToInt(Usuario::getPontos)
                .reduce(valorInicial, operacao);

        // OU:
        int total2 = usuarios.stream()
                .mapToInt(Usuario::getPontos)
                .reduce(0, (a, b) -> a + b);

        // OU ainda, é possível utilizar a classe Integer que possui um método sum
        int total3 = usuarios.stream()
                .mapToInt(Usuario::getPontos)
                .reduce(0, Integer::sum);

        // Com isso é possível obter uma multiplicação:
        int multiplicacao = usuarios.stream()
                .mapToInt(Usuario::getPontos)
                .reduce(1, (a, b) -> a * b);

        // 8.6 trabalhando com Iterators
        // É possível percorrer uma Stream através de um Iterable
        Iterator<Usuario> i = usuarios.stream().iterator();

        //  No java 8 também já um método para percorer um Iterable:
        usuarios.stream().forEachOrdered(System.out::println);

        // Mas quando utilizar um Iterable se existe forEach em Collections e Stream?
        // ->> Quando queremos modificar os objetos de uma Stream
        /**
         * Existem vários métodos para serem usados e ainda outro ponto
         * relevante: como alguns Streams podem ser originados de recursos de
         * IO, ele implementa a interface AutoCloseable e possui o close. Um
         * exemplo é usar os novos métodos da classe java.nio.file.Files,
         * incluída no Java 7 e com novidades no Java 8, para gerar um Stream.
         * Nesse caso, é fundamental tratar as exceções e o finally, ou então
         * usar o try with resources. Em outras situações, quando sabemos que
         * estamos lidando com streams gerados por coleções, essa preocupação
         * não é necessária.
         */
        // 8.7 Streams promitivos e infinitos
        /**
         * O IntStream, o LongStream e o DoubleStream possuem operações
         * especiais e que são importantes. At é mesmo o iterator deles devolvem
         * Iterators diferentes. No caso do IntStream, é o
         * PrimitiveIterator.OfInt, que implementa Iterator<Integer> mas que,
         * além de um next que devolve um Integer fazendo o boxing, também
         * possui o nextInt. Te m ainda métodos de factory, como
         * IntStream.range(inicio, fim).
         */
        //8.8 Praticando com nio
        /**
         * A classe java.nio.file.Files entrou no Java 7 para facilitar a
         * manipulação de arquivos e diretórios, trabalhando com a interface
         * Path. No Java 8 passou a trabalhar também com Stream
         */
        try {
            Files.list(Paths.get("./br/com/casadocodigo/java8")).forEach(System.out::println);

            // Filtrar apenas alguns arquivos:
            Files.list(Paths.get("./br/com/casadocodigo/java8"))
                    .filter(p -> p.endsWith(".java"))
                    .forEach(System.out::println);

            // O fileLines le todo o conteúdo do arquivo
            // O código não compila porque lança exception dentro do lambda
            //Files.list(Paths.get("./br/com/casadocodigo/java8"))
            //        .filter(p -> p.endsWith(".java"))
            //        .map(p -> Files.lines(p))
            //        .forEach(System.out::println);
            // Deve ser feito assim, foi criado a função lines static ali em baixo.
            Files.list(Paths.get("./br/com/casadocodigo/java8"))
                    .filter(p -> p.toString().endsWith(".java"))
                    .map(p -> lines(p))
                    .forEach(System.out::println);
            
            

        } catch (IOException e) {
            System.out.println("erro");
        }

        // 8.9 O código não compila
    }

    static Stream<String> lines(Path p) {
        try {
            return Files.lines(p);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
